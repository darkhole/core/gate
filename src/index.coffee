###
# dark hole gateway
###

# packages
execa  = require 'execa'
io     = require 'socket.io'
yaml   = require 'yamljs'
moment = require 'moment'
fs     = require 'fs'
path   = require 'path'

# core libs
Keys            = require '@dark-hole/keys'
Transaction     = require './lib/_transaction'
Stack           = require './lib/_stack'
AgentCollection = require './lib/_agentCollection'

# environment variables
TIMEOUT = process.env.TIMEOUT || 5
TICK    = process.env.TICK || 30000

class Gate extends io

  constructor : ( srv, opts ) ->

    return throw new Error 'trusted and pending approbation agents file are required' if not opts?.trusted or not opts?.pending

    super srv, opts

    # recuperate opts
    @_keys     = opts?.keys
    @_passport = opts.passport if opts.passport

    # load agents list
    @_trusted = AgentCollection.fromFile opts.trusted
    @_pending = AgentCollection.fromFile opts.pending

    # init transaction lists
    @_stack = new Stack()

    # ticker
    @tick()

    # calculate timeout
    @extendTimeout()

    it = @

    @on 'connection', ( socket ) ->
      socket.emit 'keys', it.getPublicKey()

      socket.on 'disconnect', ->
        if socket._name
          it.removeAgent socket
          console.log '--> agent', socket._name, 'disconnected'
        else
          console.log '--> client disconnected'

      # register
      socket.on 'register', ( data ) ->
        return socket.emit 'register:error', 'name and publicKey are required' if not data?.name or not data?.publicKey

        isValidAgent = it._trusted.validAgent data
        if isValidAgent
          console.log '<-- agent', data.name, 'connected'
          it.registerAgent socket, data
          delete it.loading?[ data.name ]
        else
          it._pending.add data
          socket.emit 'untrusted'

      # end reply to client
      socket.on 'end:error', ( data ) ->

        transaction= it._stack.get data
        if not transaction
          return console.error 'FATAL ERROR: transaction not in doing after ends'

        # remove from list
        it._stack.remove transaction

        # send reply
        client = it.getClient transaction.getSocketId()
        client?.emit transaction.errorEventName(), data.err

      socket.on 'end:success', ( data ) ->
        transaction = it._stack.get data

        if not transaction
          return console.error 'FATAL ERROR: transaction not in doing after ends'

        # forward
        if transaction.__forward
          pack =
            __name : transaction.__forward
            __body : data.result
          forward = new Transaction pack, transaction
          it.process forward
        # send reply
        else
          # remove from list
          it._stack.remove transaction
          # send reply
          client = it.getClient transaction.getSocketId()
          client?.emit transaction.successEventName(), data.result

      # encrypt
      _emit = socket.emit
      socket.emit = ->
        if socket._key
          arguments[1] = socket._key.encrypt arguments[1], 'base64' if arguments[1]
        _emit.apply socket, arguments

      # decrypt
      socket.use it.decrypt()

      # forward
      socket.use it.forward socket

  ###
  # protocol
  ###
  forward : ( socket ) ->
    it = @
    ( packet, next ) ->
      _event = packet[0]
      body   = packet[1]
      token  = packet[2]
      # return if protocol event
      return next() if _event in [ 'register', 'ping', 'pong', 'disconnect', 'end:error', 'end:success' ]

      # pack and process
      pack =
        __name : _event
        __body : body
      if not body?.__id
        if it._passport
          pack =
            __name    : it._passport
            __params  : body
            __forward : _event
            __token   : token
        transaction = new Transaction pack, null, socket.id
        if it._passport
          transaction.__genesis =
            __name : _event
            socket : socket.id
            params : body
        it.process transaction
      else
        parent = it._stack.get body
        if not parent
          return console.error 'FATAL ERROR', pack
        transaction = new Transaction pack, parent
        it.process transaction

      next()

  process : ( transaction ) ->
    @extendTimeout()

    # validate service
    if not @_trusted.validService( transaction.getName() )
      client = @getClient transaction.getSocketId()
      @_stack.remove transaction
      return client?.emit 'invalid:service', transaction.getName()

    # register transaction
    @_stack.push transaction

    group = @getAgents transaction.getName()

    if not group
      @loadService transaction.getName()
    else
      @_process group[0], transaction

  _process : ( socket, transaction ) ->
    socket.emit 'exec', transaction

  ###
  # docker
  ###
  loadService : ( name ) ->
    @loading = @loading || {}
    return if @loading[ name ]

    @loading[ name ] = true

    group   = @_trusted.getGroup name
    _path   = group[0].compose
    name    = @parseServiceName name

    try
      compose = yaml.load _path
    catch e
      return false

    if compose?.services?[ name ]
      execa 'docker-compose', [ '-f', _path, 'up', '-d', name ]
        .then ( result ) ->
          console.log 'agent', name, 'loaded'
        .catch ( error ) ->
          console.error 'can not run agent', name

  suspend : ->
    for k, v of @_agents
      group = @_trusted.getGroup k
      for i in group
        @stopService i.compose, @parseServiceName k

  stopService : ( _path, name ) ->
    try
      fs.accessSync path.resolve( _path ), fs.constants.F_OK
      execa 'docker-compose', [ '-f', _path, 'stop', name ]
        .then ( result ) ->
          console.log 'agent', name, 'stop'
        .catch ( error ) ->
          console.error error
    catch e

  parseServiceName : ( name ) ->
    name.toLowerCase().replace /:/g, '_'

  ###
  # agent administration
  ###
  getAgents : ( name ) -> @_agents?[ name ]

  removeAgent : ( socket ) ->
    item  = ( @_agents[ socket._name ].filter ( el ) -> el.id is socket.id )[0]
    index = @_agents[ socket._name ].indexOf item
    @_agents[ socket._name ].splice index, 1
    delete @_agents[ socket._name ] if @_agents[ socket._name ].length is 0

  registerAgent : ( socket, data ) ->
    # init empty agents
    @_agents = @_agents || {}
    # init empty agent group
    @_agents[ data.name ] = @_agents[ data.name ] || []
    # save to @_agents
    socket._key  = Keys.from data.publicKey
    socket._name = data.name
    @_agents[ data.name ].push socket
    socket.emit 'register:success', 'hello world'

    transactions = @_stack.filter '__name', data.name

    for transaction in transactions
      @process transaction

  ###
  # io helpers
  ###
  getClient : ( id ) ->
    @sockets.clients().connected[ id ]

  ###
  # encryption helpers
  ###
  getPublicKey : -> @_keys?.exportKey 'public'

  decrypt : ->
    it = @
    ( packet, next ) ->
      return next() if packet[1] instanceof Object
      # try decrypt as json
      try
        message = it._decrypt packet[1], 'json'
      catch e
        # try decrypt as text
        try
          message = it._decrypt packet[1], 'utf8'
        catch e
          message = packet[1]
      packet[1] = message if packet[1]
      next()

  _decrypt : ( message, encode ) ->
    @_keys?.decrypt message, encode

  # timeout
  extendTimeout : ->
    @_timeout = moment().add TIMEOUT, 'minutes'

  ###
  # garbage collector
  ###
  tick : ->
    it = @
    setTimeout ->
      # print task
      console.log 'STACK', it._stack if it._stack.count() isnt 0
      # check if timeout
      diff = moment().diff it._timeout, 'seconds'
      return it.tick() if not it._agents
      if diff >= 0 and it._stack.count() is 0 and Object.keys( it._agents ).length isnt 0
        it.suspend()
      it.tick()
    , TICK

module.exports = Gate

