class Stack

  constructor : ( @_stack = [], @_index = '__id' ) ->

  count : -> @_stack.length

  index : ( item ) ->
    @_stack.indexOf @get item

  filter : ( field, value ) ->
    @_stack.filter ( el ) -> el[ field ] is value

  get : ( item ) ->
    it = @
    ( @_stack.filter ( el ) -> el[ it._index ] is item[ it._index ] ).shift()

  exists : ( item ) ->
    return true if @get( item )
    false

  push : ( item ) ->
    if not @exists( item )
      @_stack.push item
    else
      @update item
    true

  update : ( item ) ->
    return false if not @exists( item )
    @_stack[ @index item ] = item
    true

  remove : ( item ) ->
    return false if not @exists( item )
    @_stack.splice @index( item ), 1
    true

module.exports = Stack

