fs   = require 'fs'
path = require 'path'

class AgentCollection

  constructor : ( @_file, @_collection = {} ) ->

    throw new Error 'file path is required' if not @_file

    @save()
    @watch()

  getGroup : ( name ) ->
    @_collection[ name ]

  add : ( data ) ->
    # init if not exists
    @_collection[ data.name ] = @_collection[ data.name ] || []
    exists = ( @_collection[ data.name ].filter ( el ) -> el.publicKey is data.publicKey )[0]
    if not exists
      @_collection[ data.name ].push
        compose   : data.compose
        publicKey : data.publicKey

    @save()

  remove : ( data ) ->
    agent = ( @_collection[ data.name ]?.filter ( el ) -> el.publicKey is data.publicKey )[0]
    if agent
      index = @_collection[ data.name ].indexOf agent
      @_collection[ data.name ].splice index, 1
      if @_collection[ data.name ].length is 0
        delete @_collection[ data.name ]

  @fromFile : ( file ) ->
    new AgentCollection file, AgentCollection._read file

  @_read : ( file ) ->
    try
      data = fs.readFileSync file
      return JSON.parse data
    catch e
      return {}

  validService : ( name ) ->
    if @_collection[ name ] then true else false

  validAgent : ( data ) ->
    group = @_collection[ data.name ]
    return false if not group
    agent = ( group.filter ( el ) -> el.publicKey is data.publicKey )[0]
    return true if agent
    false

  save : ->
    fs.writeFileSync @_file, JSON.stringify( @_collection, null, 2 )

  watch : ->
    it    = @
    parts = path.parse @_file
    fs.watch parts.dir, ( ev, filename ) ->
      if filename.indexOf( parts.base ) isnt -1
        it._collection = AgentCollection._read it._file

module.exports = AgentCollection

