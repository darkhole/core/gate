# packages
rs = require 'randomstring'

class Transaction

  constructor : ( packet, parent, socketId ) ->

    # clear body
    params = packet.__body
    delete packet.__body

    # assign packet and params
    Object.assign @, packet, params
    # generate __id if not exists
    if not parent
      @__id = rs.generate length : 24
      @__genesis =
        __name : @__name
        socket : socketId
        params : params
      @__history = [
        {
          __name : @__name
          params : params
        }
      ]
    else
      @__id = parent.__id
      @__genesis = parent.__genesis
      @__history = parent.__history
      @__history.push
        __name : @__name
        params : params

  getSocketId : -> @__genesis.socket
  
  getName : -> @__name

  errorEventName : -> @__genesis.__name + ':error'
  successEventName : -> @__genesis.__name + ':success'

module.exports = Transaction

